too-many-rollup-variables

If you have a project with more than 62 variables that are named the same thing, Rollup overwrites them.

## Instructions

1. `npm install && npm run generate && npm run bundle`
2. The output should be apparent in `browser.js` (note lines 1/3 and lines 249/251).
    * You can also open index.html to see the outputs in a browser console.

## Supplementary

If you change `const FILES = 63` to `const FILES = 62` in `generate.js`, the problem disappears.